/*
** proxy.c -- a stream socket client demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

//#define PORT "3490"  // the port users will be connecting to

#define BACKLOG 10   // how many pending connections queue will hold
#define MAXDATASIZE 10000

#include "proxy_client.c"
#include "buffer_handler.c"
// $ cc -o proxy proxy.c -lnsl -lsocket -lresolv
// $ cc -o proxy proxy.c -lc -lresolv
  //string hostname("zaza5.ida.liu.se");
// ifconfig -a inet | awk '/inet/ {print $2}' 


void sigchld_handler(int s)
{
  while(waitpid(-1, NULL, WNOHANG) > 0);
}

// get sockaddr, IPv4 or IPv6:
/*void *get_in_addr(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET) {
    return &(((struct sockaddr_in*)sa)->sin_addr);
  }

  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}*/

int sendall(int s, char *buf, int *len) 
  {
    int total = 0;  // how many bytes we've sent
    int bytesleft = *len; //how many we have left to send
    int n;

    while (total < *len) {
      n = send(s, buf+total, bytesleft, 0);
      if (n==-1) { break; }
      total += n;
      bytesleft -= n;
    }

    *len = total; // return number actually sent here

    return n==-1?-1:0; // return -1 on failure, 0 on success
  }

    // to pack/unpack the data, 
  #define pack754_32(f) (pack754((f), 32, 8))
  #define pack754_64(f) (pack754((f), 64, 11))
  #define unpack754_32(i) (unpack754((i), 32, 8))
  #define unpack754_64(i) (unpack754((i), 64, 11))

  uint64_t pack754(long double f, unsigned bits, unsigned expbits)
  {
      long double fnorm;
      int shift;
      long long sign, exp, significand;
      unsigned significandbits = bits - expbits - 1; // -1 for sign bit

      if (f == 0.0) return 0; // get this special case out of the way

      // check sign and begin normalization
      if (f < 0) { sign = 1; fnorm = -f; }
      else { sign = 0; fnorm = f; }

      // get the normalized form of f and track the exponent
      shift = 0;
      while(fnorm >= 2.0) { fnorm /= 2.0; shift++; }
      while(fnorm < 1.0) { fnorm *= 2.0; shift--; }
      fnorm = fnorm - 1.0;

      // calculate the binary form (non-float) of the significand data
      significand = fnorm * ((1LL<<significandbits) + 0.5f);

      // get the biased exponent
      exp = shift + ((1<<(expbits-1)) - 1); // shift + bias

      // return the final answer
      return (sign<<(bits-1)) | (exp<<(bits-expbits-1)) | significand;
  }

  long double unpack754(uint64_t i, unsigned bits, unsigned expbits)
  {
      long double result;
      long long shift;
      unsigned bias;
      unsigned significandbits = bits - expbits - 1; // -1 for sign bit

      if (i == 0) return 0.0;

      // pull the significand
      result = (i&((1LL<<significandbits)-1)); // mask
      result /= (1LL<<significandbits); // convert back to float
      result += 1.0f; // add the one back on

      // deal with the exponent
      bias = (1<<(expbits-1)) - 1;
      shift = ((i>>significandbits)&((1LL<<expbits)-1)) - bias;
      while(shift > 0) { result *= 2.0; shift--; }
      while(shift < 0) { result /= 2.0; shift++; }

      // sign it
      result *= (i>>(bits-1))&1? -1.0: 1.0;

      return result;
  }





int main(int argc, char *argv[])
{
  int sockfd, new_fd, numbytes, server_sockfd;  // listen on sock_fd, new connection on new_fd
  struct addrinfo hints, *servinfo, *p;
  struct sockaddr_storage their_addr; // connector's address information
  socklen_t sin_size;
  struct sigaction sa;
  int yes=1;
  char s[INET6_ADDRSTRLEN];
  char buf[MAXDATASIZE];
  // pointer for getting HTTP HOST name from buffer
  char *host_start, *host_end;
  char *connection_start, *connection_end;
  char content[MAXDATASIZE];
  char http_get[MAXDATASIZE];
  int rv;
  char *port_nr = argv[1];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;


  // initial fucntion, create struct
  if ((rv = getaddrinfo(NULL, port_nr, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }

  // loop through all the results and bind to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((sockfd = socket(p->ai_family, p->ai_socktype,
        p->ai_protocol)) == -1) {
      perror("server: socket");
      continue;
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
        sizeof(int)) == -1) {
      perror("setsockopt");
      exit(1);
    }

    if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(sockfd);
      perror("server: bind");
      continue;
    }

    break;
  }

    if (p == NULL)  {
    fprintf(stderr, "proxy_server: failed to bind\n");
    return 2;
  }


  freeaddrinfo(servinfo); // all done with this structure


  if (listen(sockfd, BACKLOG) == -1) {
    perror("listen");
    exit(1);
  }
/*
  sa.sa_handler = sigchld_handler; // reap all dead processes
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;
  if (sigaction(SIGCHLD, &sa, NULL) == -1) {
    perror("sigaction");
    exit(1);
  }
*/
  printf("proxy server: waitingggg for connections...\n");
  printf("somehting \n");

  while(1) {  // main loop
//    printf("NEW");
//    fflush(stdout);
    sin_size = sizeof their_addr;
    new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
    if (new_fd == -1) {
      perror("accept");
      fflush(stdout);
      continue;
    }
    printf("NEW");
    fflush(stdout);

    // convert IP address to human readable
    inet_ntop(their_addr.ss_family,
      get_in_addr((struct sockaddr *)&their_addr),
      s, sizeof s);
    printf("server: got connection from %s\n", s);


    if (!fork()) { // this is the child process
      close(sockfd); // child doesn't need the listener
      if ((numbytes = recv(new_fd, buf, MAXDATASIZE-1, 0)) == -1)
        perror("proxy_recv");
      // shut down the buffer
      buf[numbytes]='\0';


      printf("\nGet: %s\n",buf);


      // get host name from HTTP GET buffer
      host_start = strstr(buf,"Host: ");
      host_start += 6;
      host_end = strstr(host_start, "\r\n");
      char host_name [1023];
      strncpy(host_name, host_start, host_end - host_start);
      host_name[(host_end - host_start)+1] = '\0';

      // replace proxy-connection to Connection: Close
      
      //buf.replace(line.find("Proxy-Connection: keep-alive"), 28, "Connection: Close"))

      char *tempStr[2];
      tempStr[1] = host_name;
      tempStr[0] = buf;



      //strncat (buf, "Connection: Close\r\n", 18);
      server_sockfd = proxy_client(tempStr);

      // send HTTP GET(tempStr) to server and get respond data back from server
      // save it within "content", return with content size.
      buffer_handler(new_fd, server_sockfd, tempStr, content);
 
      
      exit(0);  
    }



  close(new_fd);  // parent doesn't need this
}
return 0;

}
