
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>

void close_connection(int sockfd) {
	close(sockfd);
}

void buffer_handler (int new_fd, int sockfd, char *argv[], char buf[]){
	int numbytes_send;
	int numbytes_recv;
	char* res;
	char text_buffer[MAXDATASIZE];
	bool if_text = false;
	bool if_badcontent = false;
	std::string buffer_s;

	// send HTTP GET message from proxy to server
	if ((numbytes_send = send(sockfd, argv[0], (strlen(argv[0])), 0)) == -1) {
	    perror("handler_send");
	    exit(1);
	}

	if ((numbytes_recv = recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1) {
		    perror("handler_recv");
		    exit(1);
		}

	buf[numbytes_recv] = '\0';
	printf("\n#### BUFFER IN : %s\n", buf);
	std::string first_sbuf = buf;
	int is_text = first_sbuf.find("Content-Type: text");	
	int is_encode = first_sbuf.find("Content-Encoding");

	// if we recieved a not encoded text buffer?
	if (is_text > 0 && is_encode <= 0) {
		if_text = true;
	}

	// if it is text, then check if there is any inapproperate words
	if (if_text) {
		buf[numbytes_recv] = '\0';
		text_buffer[0] = '\0';
		strcat(text_buffer, buf);
		while ((numbytes_recv = recv(sockfd, buf, MAXDATASIZE-1, 0)) > 0) {
			buf[numbytes_recv] = '\0';
			strcat(text_buffer, buf);
		} 

		std::string stext_buffer = text_buffer;

	    int inapproperate_pos1 = stext_buffer.find("SpongeBob");
      	int inapproperate_pos2 = stext_buffer.find("Britney Spears");
      	int inapproperate_pos3 = stext_buffer.find("Paris Hilton");
      	int inapproperate_pos4 = stext_buffer.find("Norrköping");
      	int is_badcontent = inapproperate_pos1 + inapproperate_pos2 + 
      			inapproperate_pos3 + inapproperate_pos4;

		if (is_badcontent > 0) {

			buffer_s = "HTTP/1.1 302\r\nLocation: http://www.ida.liu.se/~TDTS04/labs/2011/ass2/error2.html\r\n\r\n";
        	strcpy(buf, buffer_s.c_str());

        	if ((send(new_fd, buf, strlen(buf), 0)) == -1) {
           		perror("send");
          	exit(1);
          	}
		} else {
			if ((send(new_fd, text_buffer, strlen(text_buffer), 0)) == -1) {
           		perror("send");
          	exit(1);
			}
		}

	} else {

		if ((numbytes_send = send(new_fd, buf, numbytes_recv, 0)) == -1) {
        	perror("send");
        	exit(1);
  	  	}

		while ((numbytes_recv =recv(sockfd, buf, MAXDATASIZE-1, 0)) > 0) {
			// get data from server back to our proxy
			buf[numbytes_recv] = '\0';
			// send the content to browser
			if ((numbytes_send = send(new_fd, buf, numbytes_recv, 0)) == -1) {
        	 	perror("send");
        		exit(1);
      		}

		}


	}

	close(new_fd);
}


