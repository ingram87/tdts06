
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>

void close_connection(int sockfd) {
	close(sockfd);
}

void buffer_handler (int new_fd, int sockfd, char *argv[], char buf[]){

	//char buf[MAXDATASIZE];
	//char* buf = (char *)(1000);
	int numbytes_send, numbytes_recv;
	char* res;

	//printf("%zd\n", strlen(argv[0]));
	//printf("\nbuffer : %s\n\n", argv[0]);
	// send HTTP GET message from proxy to server
	if ((numbytes_send = send(sockfd, argv[0], (strlen(argv[0])), 0)) == -1) {
	    perror("handler_send");
	    exit(1);
	}

	printf ("buffer_handler send\n");

	if ((numbytes_recv = recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1) {
		    perror("handler_recv");
		    printf("%d\n", sockfd);
		    exit(1);
		}


	if ((numbytes_send = send(new_fd, buf, numbytes_recv, 0)) == -1) {
         perror("send");
        exit(1);
      }

	while ((numbytes_recv =recv(sockfd, buf, MAXDATASIZE-1, 0)) > 0) {
		// get data from server back to our proxy

		// send the content to browser
		if ((numbytes_send = send(new_fd, buf, numbytes_recv, 0)) == -1) {
        	 perror("send");
        	exit(1);
      	}

	}
	close(new_fd);

	printf ("buffer_handler recv\n");
	printf("\nBUFFER IN buffer_handler: %s\n", buf);

}


